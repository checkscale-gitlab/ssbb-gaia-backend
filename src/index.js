import config from './config/vars';
import app from './config/express';
import mongoose from './config/mongoose';
import models from './api/models/index.model';
// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign
const debug = require('debug')('express-sequelize');

const { port, env } = config;

// open mongoose connection
mongoose.connect();


// get models
models.sequelize.sync();

// listen to requests
app.listen(port, () => debug(`server started on port ${port} (${env})`));

/**
* Exports express
* @public
*/
export default app;
