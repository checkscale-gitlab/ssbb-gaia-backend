#!/bin/bash
docker build -t danielfsousa/ssbb-gaia-backend .
docker push danielfsousa/ssbb-gaia-backend

ssh deploy@$DEPLOY_SERVER << EOF
docker pull danielfsousa/ssbb-gaia-backend
docker stop api-boilerplate || true
docker rm api-boilerplate || true
docker rmi danielfsousa/ssbb-gaia-backend:current || true
docker tag danielfsousa/ssbb-gaia-backend:latest danielfsousa/ssbb-gaia-backend:current
docker run -d --restart always --name api-boilerplate -p 1207:1207 danielfsousa/ssbb-gaia-backend:current
EOF
